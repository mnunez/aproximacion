#!/usr/bin/env pythoh3


# MARTA NÚÑEZ GARCÍA


'''
Cálculo del número óptimo de árboles.
'''

import sys

# Función "compute_trees": calcula la producción del número de árboles.
def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production


# Función "compute_all": recibe como argumento el número de árboles mínimo y máximo y calcula las producciones para
# cualquier número de árboles entre ese mínimo y ese máximo, devolviendo una lista de tuplas (número de árboles,
# producción).
def compute_all(min_trees, max_trees):

    productions = []
    for i in range(min_trees, max_trees + 1):
        product = compute_trees(i)
        productions.append((i, product))
    return productions


# Función "read_arguments": llamada de argumentos (números enteros). Comprueba que el programa ha sido llamado con
# el número adecuado de argumentos.
def read_arguments():

    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])

    except:
        sys.exit("Usage: python3 aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")

    return base_trees, fruit_per_tree, reduction, min, max


# Función "main": usa tres variables globales (base_trees, fruit_per_tree y reduction). Llama primero a la función
# "read_arguments" para obtener los argumentos de la línea de comandos como números enteros. Después llama a la
# función "compute_all" para obtener una lista de tuplas. Finalmente, imprime los valores de dicha lista y calcula e
# imprime la mejor producción.
def main():

    global base_trees
    global fruit_per_tree
    global reduction

    best_production = 0
    best_trees = 0
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)
    for i in range(min, max + 1):
        product = compute_trees(i)
        print(f"{i} {product}")
        if product > best_production:
            best_production, best_trees = product, i

    print(f"Best production: {best_production}, for {best_trees} trees.")



if __name__ == '__main__':
    main()